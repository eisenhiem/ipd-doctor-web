import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeMedicationRoutingModule } from './home-medication-routing.module';
import { HomeMedicationComponent } from './home-medication.component';


@NgModule({
  declarations: [
    HomeMedicationComponent
  ],
  imports: [
    CommonModule,
    HomeMedicationRoutingModule
  ]
})
export class HomeMedicationModule { }
