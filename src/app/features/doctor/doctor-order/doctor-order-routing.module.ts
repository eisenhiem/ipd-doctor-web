import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DoctorOrderComponent } from './doctor-order.component';

const routes: Routes = [{ path: '', component: DoctorOrderComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DoctorOrderRoutingModule { }
