import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DoctorOrderRoutingModule } from './doctor-order-routing.module';
import { DoctorOrderComponent } from './doctor-order.component';
import { NgZorroModule } from 'src/app/ng-zorro.module';
import { NzAutocompleteModule } from 'ng-zorro-antd/auto-complete';
import { SharedModule } from 'src/app/shared/shared.module';



@NgModule({
  declarations: [
    DoctorOrderComponent
  ],
  imports: [
    CommonModule,
    DoctorOrderRoutingModule,
    NgZorroModule,
    NzAutocompleteModule,
    SharedModule
  ]
})
export class DoctorOrderModule { }
