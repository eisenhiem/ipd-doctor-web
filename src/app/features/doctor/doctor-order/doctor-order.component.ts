import { Component, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DateTime } from 'luxon';
import { DoctorService } from '../services/doctor.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { VariableShareService } from '../../../core/services/variable-share.service';
import * as moment from 'moment';
import * as _ from 'lodash';
import { number } from 'echarts';

interface Option {
  label: string;
  name: string;
  id: string;
  code: string;
  default_usage: string;
}
interface OptionItems {
  id: string;
  name: string;
  default_usage: string;
  item_type_id: string;
}
interface OptionDrugsUsage {
  id: string;
  name: string;
  default_usage: string;
}
@Component({
  selector: 'app-doctor-order',
  templateUrl: './doctor-order.component.html',
  styleUrls: ['./doctor-order.component.css'],
})
export class DoctorOrderComponent {
  query: any = '';
  dataSet: any[] = [];
  loading = false;

  total = 0;
  pageSize = 20;
  pageIndex = 1;
  offset = 0;
  user_login_name: any;

  progressValueS?: string;
  progressValueO?: string;
  progressValueA?: string;
  progressValueP?: string;
  onedayValue?: string;
  continueValue?: string;

  progressTags = ['progressValue'];
  onedayTags = ['onedayValue'];
  continueTags = ['onedayValue'];

  inputVisible = false;
  inputValue?: string = '';

  patientList: any;
  orderList: any;
  drugList: any;

  hn: any;
  an: any;

  title: any;
  fname: any;
  lname: any;
  gender: any;
  age: any;
  address: any;
  phone: any;

  admit_id: any;
  doctor_order_date: any;
  doctor_order_time: any;
  doctor_order_by: any;
  is_confirm: any;

  subjective: any;
  objective: any;
  assertment: any;
  plan: any;
  note: any;

  order_type_id: any;
  item_type_id: any;
  item_id: any;
  item_name: any;
  medicine_usage_code: any;
  medicine_usage_extra: any;
  quantity: any;

  food_id: any;
  start_date: any;
  start_time: any;
  end_date: any;
  end_time: any;

  is_vital_sign: any;

  progress_note: any;
  progress_note_subjective: any[] = [];
  progress_note_objective: any[] = [];
  progress_note_assertment: any[] = [];
  progress_note_plan: any[] = [];
  oneDay: any[] = [];
  continue: any[] = [];
  orders: any[] = [];
  ordersOneday: any[] = [];
  continues: any[] = [];

  medicine: any[] = [];

  progress_note_id: any;

  drugname: any;

  @ViewChild('inputElement', { static: false }) inputElement?: ElementRef;
  queryParamsData: any;
  value?: string;
  inputValue1?: string;
  // options: string[] = [];

  inputValueOneday: Option = {
    label: 'รายการ',
    name: 'รายการ',
    id: '0',
    code: '',
    default_usage: '',
  };
  // optionsOnedays: Option[] = [
  //   { label: 'Para', name: 'Para', id: '1', },
  //   { label: 'Amoxy', name: 'Amoxy', id: '2', },
  //   { label: 'Diclo', name: 'Diclo', id: '3', },
  //   { label: 'Diclo', name: 'Diclo', id: '3', },
  //   { label: 'Diclo', name: 'Diclo', id: '3', },
  //   { label: 'Diclo', name: 'Diclo', id: '3', },
  //   { label: 'Diclo', name: 'Diclo', id: '3', },
  //   { label: 'Diclo', name: 'Diclo', id: '3', },

  // ];
  optionsOneday: Option[] = [];
  // inputValue = '';

  // options: Option[] = [
  //   { label: 'Para', name: 'Para', id: '1' },
  //   { label: 'Amoxy', name: 'Amoxy', id: '2' },
  //   { label: 'Diclo', name: 'Diclo', id: '3' },
  //   { label: 'Ibuprofen', name: 'Ibuprofen', id: '4' },
  //   { label: 'Aspirin', name: 'Aspirin', id: '5' },
  //   { label: 'Naproxen', name: 'Naproxen', id: '6' },
  // ];
  optionsAllItems: OptionItems[] = [];

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private doctorService: DoctorService,
    private message: NzMessageService,
    private modal: NzModalService,
    private variableShareService: VariableShareService
  ) {
    let jsonString: any =
      this.activatedRoute.snapshot.queryParamMap.get('data');
    const jsonObject = JSON.parse(jsonString);
    this.queryParamsData = jsonObject;
    console.log(this.queryParamsData);
    // genItems();
  }

  ngOnInit(): void {
    this.closeSidebar();
    // this.getList();
    this.getPatient();
    this.getDoctorOrder();
    this.getLookupMedicine();
    // this.getLabItems();
    // this.getXrayItems();
    // this.getProcedureItems();

    // this.printData();
  }
  genItems() {
    for (let i = 1; i <= 10000; i++) {
      this.optionsOneday.push({
        label: `Drug ${i}`,
        name: `Drug ${i}`,
        id: `${i}`,
        code: `${i}`,
        default_usage: `Usage ${i}`,
      });
    }
    console.log(this.optionsOneday);
  }
  compareFun = (o1: Option | string, o2: Option): boolean => {
    if (o1) {
      // console.log(o1);
      // console.log(this.inputValueOneday);
      return typeof o1 === 'string' ? o1 === o2.label : o1.name === o2.name;
    } else {
      return false;
    }
  };
  //การดึงค่าจาก  html tag  ---- (input)="onInput($event)"
  // onInput(e: Event): void {
  //   const value = (e.target as HTMLInputElement).value;
  //   if (!value || value.indexOf('@') >= 0) {
  //     this.options = [];
  //   } else {
  //     this.options = ['gmail.com', '163.com', 'qq.com'].map(domain => `${value}@${domain}`);
  //   }
  // }
  filteredOptions: Option[] = [];

  // onInputValueChange(value: string): void {
  //   this.filteredOptions = this.filterOptions(value);
  // }

  // filterOptions(value: string): Option[] {
  //   const filterValue = value.toLowerCase();
  //   return this.options.filter(
  //     (option) =>
  //       option.label.toLowerCase().includes(filterValue) ||
  //       option.name.toLowerCase().includes(filterValue) ||
  //       option.id.toLowerCase().includes(filterValue)
  //   );
  // }
  ////////////////////Med oneday/////////////////
  ordersOnedayQuantity?: number;
  optionsDrugs: OptionItems[] = [];
  // optionsDrugs: OptionDrugs[] = [
  //   { usage_code: '1prn', name: 'Para', id: '1', },
  //   { usage_code: '2tp', name: 'Amoxy', id: '2', },
  //   { usage_code: '3pan', name: 'Diclo', id: '3', },
  //   { usage_code: '3tba', name: 'Ibuprofen', id: '4', },
  //   { usage_code: '1dap', name: 'Aspirin', id: '5', },
  //   { usage_code: 'im', name: 'Naproxen', id: '6', },

  // ];
  filteredOptionsItems: OptionItems[] = [];
  inputValueItems?: string;
  onInputValueChangeItems(value: string): void {
    this.filteredOptionsItems = this.filterOptionsItems(value);
    console.log(this.filteredOptionsItems);
    // console.log(this.inputValueItems);
    this.inputValueItemsUsage = this.filteredOptionsItems[0].default_usage;
  }
  filterOptionsItems(value: string): OptionItems[] {
    const filterValue = value.toLowerCase();
    return this.optionsAllItems.filter(
      (option) =>
        option.name.toLowerCase().includes(filterValue) ||
        option.id.toLowerCase().includes(filterValue)
    );
  }
  // filterOptionsItems(value: string): OptionItems[] {
  //   const filterValue = value.toLowerCase();
  //   return this.optionsDrugs.filter(
  //     (option) =>
  //       option.name.toLowerCase().includes(filterValue) ||
  //       option.id.toLowerCase().includes(filterValue)
  //   );
  // }
  // test 10000
  // filterOptionsItems(value: string): OptionDrugs[] {
  //   const filterValue = value.toLowerCase();
  //   return this.optionsOneday.filter(
  //     (option) =>
  //       option.name.toLowerCase().includes(filterValue) ||
  //       option.id.toLowerCase().includes(filterValue)
  //   );
  // }
  ////////////////////End drugs oneday/////////////////

  ////////////////////drugs oneday Usage/////////////////
  optionsDrugsUsage: OptionDrugsUsage[] = [];
  // optionsDrugsUsage: OptionDrugsUsage[] = [
  //   { usage_code: '1prn', name: '1 เม็ต เวลาปวด', id: '1', },
  //   { usage_code: '2tp', name: '2 แค็ป เช้าเย็น', id: '2', },
  //   { usage_code: '3pan', name: '3 เม็ด หลังอาหารเช้าเย็น', id: '3', },
  //   { usage_code: '3tba', name: '3 เม็ด หลังอาหารทันที', id: '4', },
  //   { usage_code: '1dap', name: '1 เม็ดตอนเช้าทุกวัน', id: '5', },
  //   { usage_code: 'im', name: 'สำหรับฉีกเข้ากล้ามเนื้อ', id: '6', },

  // ];
  filteredOptionsDrugsUsage: OptionDrugsUsage[] = [];
  inputValueItemsUsage?: string;
  onInputValueChangeDrugsUsage(value: string): void {
    this.filteredOptionsDrugsUsage = this.filterOptionsDrugsUsage(value);
    console.log(this.filteredOptionsDrugsUsage);
  }

  filterOptionsDrugsUsage(value: string): OptionDrugsUsage[] {
    const filterValue = value.toLowerCase();
    return this.optionsDrugsUsage.filter(
      (option) =>
        option.name.toLowerCase().includes(filterValue) ||
        option.id.toLowerCase().includes(filterValue)
    );
  }
  ////////////////////End drugs oneday Usage/////////////////

  logOut() {
    console.log('logOut');
    sessionStorage.setItem('token', '');
    return this.router.navigate(['/login']);
  }
  closeSidebar(): void {
    this.variableShareService.setCloseSidebar(true);
  }

  onPageIndexChange(pageIndex: any) {
    this.offset = pageIndex === 1 ? 0 : (pageIndex - 1) * this.pageSize;

    this.getList();
  }

  onPageSizeChange(pageSize: any) {
    this.pageSize = pageSize;
    this.pageIndex = 1;

    this.offset = 0;

    this.getList();
  }

  async getList() {
    const messageId = this.message.loading('Loading...').messageId;
    try {
      const _limit = this.pageSize;
      const _offset = this.offset;
      const response = await this.doctorService.getWaiting(_limit, _offset);

      const data: any = response.data;

      this.total = data.total || 1;

      this.dataSet = data.data.map((v: any) => {
        const date = v.admit_date
          ? DateTime.fromISO(v.admit_date)
              .setLocale('th')
              .toLocaleString(DateTime.DATETIME_SHORT_WITH_SECONDS)
          : '';
        v.admit_date = date;
        return v;
      });
      this.message.remove(messageId);
    } catch (error: any) {
      this.message.remove(messageId);
      this.message.error(`${error.code} - ${error.message}`);
    }
  }

  async getPatient() {
    const messageId = this.message.loading('Loading...').messageId;
    try {
      const _limit = this.pageSize;
      const _offset = this.offset;
      console.log(this.queryParamsData);

      const response = await this.doctorService.getPatientInfo(
        this.queryParamsData
      );

      const data: any = response.data;

      this.patientList = await data.data;
      //console.log(this.patientList);
      this.hn = this.patientList.hn;
      this.an = this.patientList.an;
      this.title = this.patientList.patient.title;
      this.fname = this.patientList.patient.fname;
      this.lname = this.patientList.patient.lname;
      this.gender = this.patientList.patient.gender;
      this.age = this.patientList.patient.age;
      this.address = this.patientList.address;
      this.phone = this.patientList.phone;

      this.total = data.total || 1;

      this.dataSet = data.data.map((v: any) => {
        const date = v.admit_date
          ? DateTime.fromISO(v.admit_date)
              .setLocale('th')
              .toLocaleString(DateTime.DATETIME_SHORT_WITH_SECONDS)
          : '';
        v.admit_date = date;
        return v;
      });
      this.message.remove(messageId);
    } catch (error: any) {
      this.message.remove(messageId);
      this.message.error(`${error.code} - ${error.message}`);
    }
  }

  async getLookupMedicine() {
    const messageId = this.message.loading('Loading...').messageId;
    try {
      let response = await this.doctorService.getLookupMedicine();
      this.optionsDrugs = response.data.data;
      // this.optionsDrugsUsage = response.data.data;

      console.log(this.optionsDrugs);

      this.message.remove(messageId);
      this.optionsDrugs.forEach((item) => {
        this.optionsAllItems.push(item);
      });
      this.getLabItems();
    } catch (error: any) {
      this.message.remove(messageId);
      this.message.error(`${error.code} - ${error.message}`);
    }
  }
  async getLookupMedicineUsage() {
    const messageId = this.message.loading('Loading...').messageId;
    try {
      let response = await this.doctorService.getLookupMedicine();
      this.optionsDrugs = response.data.data;
      // this.optionsDrugsUsage = response.data.data;

      // console.log(this.optionsDrugs);

      this.message.remove(messageId);
    } catch (error: any) {
      this.message.remove(messageId);
      this.message.error(`${error.code} - ${error.message}`);
    }
  }
  optionsLabs: OptionItems[] = [];

  async getLabItems() {
    const itemTypeID = 1;
    console.log('getLabItems');

    try {
      let response = await this.doctorService.getItems(itemTypeID);
      // console.log(response);
      this.optionsLabs = response.data.data;

      // console.log(this.optionsLabs);

      this.optionsLabs.forEach((item) => {
        this.optionsAllItems.push(item);
      });

      this.getXrayItems();

      
    } catch (error: any) {
      this.message.error(`${error.code} - ${error.message}`);
    }
  }

  optionsXray: OptionItems[] = [];

  async getXrayItems() {
    const itemTypeID = 2;
    console.log('getXrayItems');

    try {
      let response = await this.doctorService.getItems(itemTypeID);
      // console.log(response);
      this.optionsXray = response.data.data;

      // console.log(this.optionsXray);

      this.optionsXray.forEach((item) => {
        this.optionsAllItems.push(item);
      });
      console.log(this.optionsAllItems);
      // this.getProcedureItems();
    } catch (error: any) {
      this.message.error(`${error.code} - ${error.message}`);
    }
  }

  optionsProcedure: OptionItems[] = [];

  async getProcedureItems() {
    const itemTypeID = 3;
    console.log('getProcedureItems');
    try {
      let response = await this.doctorService.getItems(itemTypeID);
      // console.log(response);
      this.optionsProcedure = response.data.data;

      // console.log(this.optionsProcedure);
    } catch (error: any) {
      this.message.error(`${error.code} - ${error.message}`);
    }
  }

  async getDoctorOrder() {
    const messageId = this.message.loading('Loading...').messageId;
    try {
      const _limit = this.pageSize;
      const _offset = this.offset;
      console.log(this.queryParamsData);

      const response = await this.doctorService.getDoctorOrderById(
        this.queryParamsData
      );

      const data: any = response.data;

      this.orderList = await data.data[0];
      console.log(this.orderList);
      this.subjective = this.orderList.progress_note.subjective;
      this.objective = this.orderList.progress_note.objective;
      this.assertment = this.orderList.progress_note.assertment;
      this.plan = this.orderList.progress_note.plan;
      let _order: any = [];
      for (let d of data.data[0]) {
        _order.push(d.order);
      }
      this.orders = _order;
      // this.medicine_usage_extra=this.orderList.order.medicine_usage_extra;
      // this.quantity=this.orderList.order.quantity;
      console.log(_order);

      // this.an = this.patientList.an;
      // this.title = this.patientList.patient.title;
      // this.fname = this.patientList.patient.fname;
      // this.lname = this.patientList.patient.lname;
      // this.gender = this.patientList.patient.gender;
      // this.age = this.patientList.patient.age;
      // this.address = this.patientList.address;
      // this.phone = this.patientList.phone;

      // this.total = data.total || 1

      this.dataSet = data.data.map((v: any) => {
        const date = v.admit_date
          ? DateTime.fromISO(v.admit_date)
              .setLocale('th')
              .toLocaleString(DateTime.DATETIME_SHORT_WITH_SECONDS)
          : '';
        v.admit_date = date;
        return v;
      });
      this.message.remove(messageId);
    } catch (error: any) {
      this.message.remove(messageId);
      this.message.error(`${error.code} - ${error.message}`);
    }
  }

  preventDefault(e: Event): void {
    e.preventDefault();
    e.stopPropagation();
    console.log('tag can not be closed.');
  }

  handleCloseProgress(removedTag: {}): void {
    this.progressTags = this.progressTags.filter(
      (progressTags) => progressTags !== removedTag
    );
  }

  sliceTagNameProgress(progressTags: string): string {
    const isLongTag = progressTags.length > 20;
    return isLongTag ? `${progressTags.slice(0, 20)}...` : progressTags;
  }

  handleCloseOneday(removedTag: {}): void {
    console.log('mdkkd');
    this.onedayTags = this.onedayTags.filter(
      (onedayTags) => onedayTags !== removedTag
    );
  }

  sliceTagNameOneday(onedayTags: string): string {
    const isLongTag = onedayTags.length > 200;
    return isLongTag ? `${onedayTags.slice(0, 200)}...` : onedayTags;
  }

  handleCloseContinue(removedTag: {}): void {
    this.continueTags = this.continueTags.filter(
      (continueTags) => continueTags !== removedTag
    );
  }

  sliceTagNameContinue(continueTags: string): string {
    const isLongTag = continueTags.length > 20;
    return isLongTag ? `${continueTags.slice(0, 20)}...` : continueTags;
  }

  showInput(): void {
    this.inputVisible = true;
    setTimeout(() => {
      this.inputElement?.nativeElement.focus();
    }, 10);
  }

  // handleInputConfirm(): void {
  //   if (this.inputValue && this.tags.indexOf(this.inputValue) === -1) {
  //     this.tags = [...this.tags, this.inputValue];
  //   }
  //   this.inputValue = '';
  //   this.inputVisible = false;
  // }

  addProgressNoteS(): void {
    if (
      this.progressValueS &&
      this.progressTags.indexOf(this.progressValueS) === -1
    ) {
      this.progressTags = [...this.progressTags, this.progressValueS];
      this.progress_note_subjective = this.progressTags;
    }
    // this.progress_note_subjective.push(this.progressTags);
    // this.progress_note={this.progress_note_subjective};
    console.log(this.progress_note_subjective);
    console.log(this.progress_note);

    this.progressValueS = '';
  }
  addProgressNoteO(): void {
    console.log('addProgressNoteO');
    if (
      this.progressValueO &&
      this.progressTags.indexOf(this.progressValueO) === -1
    ) {
      this.progressTags = [...this.progressTags, this.progressValueO];
      this.progress_note_objective = this.progressTags;
    }
    this.progressValueO = '';
  }
  addProgressNoteA(): void {
    if (
      this.progressValueA &&
      this.progressTags.indexOf(this.progressValueA) === -1
    ) {
      this.progressTags = [...this.progressTags, this.progressValueA];
      this.progress_note_assertment = this.progressTags;
    }
    this.progressValueA = '';
  }
  addProgressNoteP(): void {
    if (
      this.progressValueP &&
      this.progressTags.indexOf(this.progressValueP) === -1
    ) {
      this.progressTags = [...this.progressTags, this.progressValueP];
      this.progress_note_plan = this.progressTags;
    }
    this.progressValueP = '';
  }

  addOnedayOld(): void {
    if (this.onedayValue && this.onedayTags.indexOf(this.onedayValue) === -1) {
      this.onedayTags = [...this.onedayTags, this.onedayValue];
      this.oneDay = this.onedayTags;
      let splittops = _.split(this.onedayValue, ' ');
      console.log(splittops);
      let topsarray = {
        order_type_id: 1,
        item_id: this.queryParamsData,
        item_type_id: 2,
        item_name: splittops[0],
        medicine_usage_code: splittops[1],
        medicine_usage_extra: splittops[2],
        quantity: +splittops[3],
        is_confirm: true,
      };
      console.log(topsarray);
      this.orders.push(topsarray);
      console.log(this.orders);
    }
    this.onedayValue = '';
  }
  ordersOnedayAdd(): void {
    let item_name: any = this.inputValueItems;
    let item_type_id:any = this.filteredOptionsItems[0].item_type_id
    let medicine_usage_code: any = this.inputValueItemsUsage ?? '';
    let medicine_usage_extra = 'Extra..';
    let quantity: any =this.ordersOnedayQuantity ?? '';
    let usa: any = this.filteredOptionsItems[0].item_type_id  ? '' : 'Usage:';
    let qt: any = this.filteredOptionsItems[0].item_type_id  ? '' : 'Qty:';

    

    if (item_name != '') {
      let topsarray = {
        order_type_id: 1,
        item_type_id: item_type_id,
        item_name: item_name,
        medicine_usage_code: medicine_usage_code,
        medicine_usage_extra: medicine_usage_extra,
        quantity: quantity,
        is_confirm: true,
      };

      let text = item_name + ' '+usa+' ' + medicine_usage_code + '  '+qt+'' + quantity;
      this.onedayTags.push(text);
      this.ordersOneday.push(topsarray);
      console.log(this.ordersOneday);
      this.inputValueItems = '';
      this.inputValueItemsUsage = '';
      this.ordersOnedayQuantity = undefined;
    } else {
      console.log('data is not complete');
      alert('data is not complete');
    }
  }

  addContinue(): void {
    console.log('addContinue');
    if (
      this.continueValue &&
      this.continueTags.indexOf(this.continueValue) === -1
    ) {
      this.continueTags = [...this.continueTags, this.continueValue];
      this.continue = this.continueTags;
      let splittops = _.split(this.continueValue, ' ');
      console.log(splittops);
      let topsarray = {
        order_type_id: 2,
        item_type_id: 2,
        item_id: this.queryParamsData,
        item_name: splittops[0],
        medicine_usage_code: splittops[1],
        medicine_usage_extra: splittops[2],
        quantity: +splittops[3],
        is_confirm: true,
      };
      console.log(topsarray);
      this.continues.push(topsarray);
      console.log(this.continues);
    }
    this.continueValue = '';
  }

  async saveDoctorOrder() {
    const format1 = 'YYYY-MM-DD HH:mm:ss';
    const format2 = 'YYYY-MM-DD';
    const format3 = 'HH:mm:ss';
    var date1 = new Date('2020-06-24 22:57:36');
    var date2 = new Date();
    var date3 = new Date();

    const date4 = moment(date1).format(format1);
    const date5 = moment(date2).format(format2);
    const time1 = moment(date3).format(format3);

    let data = {
      doctor_order: {
        admit_id: this.queryParamsData,
        doctor_order_date: date5,
        doctor_order_time: time1,
        doctor_order_by: this.queryParamsData,
        is_confirm: true,
      },
      progress_note: {
        subjective: this.progress_note_subjective,
        objective: this.progress_note_objective,
        assertment: this.progress_note_assertment,
        plan: this.progress_note_plan,
        note: 'test',
      },
      orders: [this.orders[0], this.continues[0]],
      order_food: {
        food_id: this.queryParamsData,
        start_date: date5,
        start_time: time1,
        end_date: date5,
        end_time: time1,
      },
      order_vital_sign: [
        {
          admit_id: this.queryParamsData,
          is_vital_sign: true,
          start_date: date5,
          start_time: time1,
          end_date: date5,
          end_time: time1,
        },
      ],
    };

    try {
      console.log(data);

      this.doctorService.saveDoctorOrder(data);
    } catch (error) {
      console.log(error);
    }
  }

  clearOneDayItemsUsage() {
    console.log('clearOneDayDrugsUsage');
    this.inputValueItemsUsage = '';
  }
  clearOneDayItems() {
    this.inputValueItems = '';
    this.filteredOptionsItems = [];
    console.log(this.filteredOptionsItems);
    console.log(this.inputValueItems);
    this.inputValueItemsUsage = '';
  }

  /////////////////////////////////////////////////

  // Function to generate a random date within a range
  randomDate(start: any, end: any) {
    return new Date(
      start.getTime() + Math.random() * (end.getTime() - start.getTime())
    );
  }

  // Function to generate random records
  generateRandomRecord(id: any) {
    const medicines = [
      'ACTIFED TAB',
      'ASPIRIN',
      'IBUPROFEN',
      'TYLENOL',
      'ZANTAC',
    ];
    const units = ['เม็ด', 'ml', 'mg'];
    const defaultUsages = ['T1', 'T2', 'T3', 'T4', 'T5'];
    const strengths = [30, 60, 90, 120, 150];

    const record = {
      code: Math.floor(Math.random() * 1000), // Random code
      create_by: null,
      create_date: this.randomDate(new Date(2023, 0, 1), new Date()),
      default_route: '',
      default_usage:
        defaultUsages[Math.floor(Math.random() * defaultUsages.length)],
      id: id,
      is_active: Math.random() < 0.5, // Random true/false
      is_ed: Math.random() < 0.5,
      medicine_type_id: 1,
      modify_by: null,
      modify_date: this.randomDate(new Date(2023, 0, 1), new Date()),
      name: medicines[Math.floor(Math.random() * medicines.length)],
      pregnant_allow: Math.random() < 0.5,
      product_cat: 1,
      provider_allow: null,
      strenght:
        strengths[Math.floor(Math.random() * strengths.length)].toFixed(2), // Random strength
      tallman: medicines[Math.floor(Math.random() * medicines.length)],
      unit: units[Math.floor(Math.random() * units.length)],
    };

    return record;
  }

  numberOfRecords: number = 20000;
  dataArrays: any = [];
  printData() {
    for (let i = 0; i < this.numberOfRecords; i++) {
      const id = `${i + 1}`;
      const record = this.generateRandomRecord(id);
      this.dataArrays.push(record);
    }

    console.log(JSON.stringify(this.dataArrays, null, 2));
  }

  ///////////////////////////////////////////////////
}
