import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorCareComponent } from './doctor-care.component';

describe('DoctorCareComponent', () => {
  let component: DoctorCareComponent;
  let fixture: ComponentFixture<DoctorCareComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DoctorCareComponent]
    });
    fixture = TestBed.createComponent(DoctorCareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
