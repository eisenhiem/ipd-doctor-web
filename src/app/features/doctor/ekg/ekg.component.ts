import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DateTime } from 'luxon';
import { DoctorService } from '../services/doctor.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import {UserProfileService} from '../../../core/services/user-profiles.service';

@Component({
  selector: 'app-ekg',
  templateUrl: './ekg.component.html',
  styleUrls: ['./ekg.component.css']
})
export class EkgComponent {
 
  query: any = '';
  dataSet: any[] = [];
  loading = false;

  total = 0;
  pageSize = 20;
  pageIndex = 1;
  offset = 0;
  user_login_name:any;
   queryParamsData: any;


  constructor (
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private doctorService: DoctorService,
    private message: NzMessageService,
    private modal: NzModalService,


  ) {

    let jsonString: any = this.activatedRoute.snapshot.queryParamMap.get('data');
    const jsonObject = JSON.parse(jsonString);
    this.queryParamsData = jsonObject;
    console.log(this.queryParamsData);
   }

  ngOnInit(): void {
 


  }
  logOut(){
    sessionStorage.setItem('token','');
    return this.router.navigate(['/login']);    
  }

  onPageIndexChange(pageIndex: any) {

    this.offset = pageIndex === 1 ?
      0 : (pageIndex - 1) * this.pageSize;

    this.getList()
  }

  onPageSizeChange(pageSize: any) {
    this.pageSize = pageSize
    this.pageIndex = 1

    this.offset = 0

    this.getList()
  }

  async getList() {
    const messageId = this.message.loading('Loading...').messageId;
    try {
      const _limit = this.pageSize;
      const _offset = this.offset;
      const response = await this.doctorService.getWaiting(_limit, _offset);

      const data: any = response.data;

      this.total = data.total || 1

      this.dataSet = data.data.map((v: any) => {
        const date = v.admit_date ? DateTime.fromISO(v.admit_date).setLocale('th').toLocaleString(DateTime.DATETIME_SHORT_WITH_SECONDS) : '';
        v.admit_date = date;
        return v;
      });
      this.message.remove(messageId);
    } catch (error: any) {
      this.message.remove(messageId);
      this.message.error(`${error.code} - ${error.message}`);
    }
  }
}

