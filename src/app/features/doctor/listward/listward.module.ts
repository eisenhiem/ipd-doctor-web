import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedModule} from '../../../shared/shared.module';

import { ListwardRoutingModule } from './listward-routing.module';
import { ListwardComponent } from './listward.component';
import { NgZorroModule } from '../../../ng-zorro.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// import {DeviceCheckComponent} from './app-device-check';




@NgModule({
  declarations: [
    ListwardComponent,
    // DeviceCheckComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    NgZorroModule,
    FormsModule,
    ReactiveFormsModule,
    ListwardRoutingModule
  ]
})
export class ListwardModule { }
