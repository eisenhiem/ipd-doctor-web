import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DateTime } from 'luxon';
import { DoctorService } from '../services/doctor.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { UserProfileService } from '../../../core/services/user-profiles.service';
import { VariableShareService } from '../../../core/services/variable-share.service';



@Component({
  selector: 'app-patientinfo',
  templateUrl: './patientinfo.component.html',
  styleUrls: ['./patientinfo.component.css']
})
export class PatientinfoComponent {
  query: any = '';
  dataSet: any[] = [];
  loading = false;

  total = 0;
  pageSize = 20;
  pageIndex = 1;
  offset = 0;
  user_login_name: any;
  queryParamsData: any;
  patientList:any;
  hn:any;
  an: any;

  title: any;
  fname: any;
  lname: any;
  gender: any;
  age: any;
  address: any;
  phone: any;

  chief_complaint:any;
	present_illness:any;
  past_history:any;

  physical_exam:any;
  body_temperature:any;
      body_weight:any;
      body_height:any;
      waist:any;
      pulse_rate:any;
      respiratory_rate:any;
      systolic_blood_pressure:any;
      diatolic_blood_pressure:any;
      oxygen_sat:any;
      eye_score:any;
      movement_score:any;
      verbal_score:any;
bmi:any;
inscl_name:any;



  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private doctorService: DoctorService,
    private message: NzMessageService,
    private modal: NzModalService,
    private variableShareService: VariableShareService



  ) {

    let jsonString: any = this.activatedRoute.snapshot.queryParamMap.get('data');
    const jsonObject = JSON.parse(jsonString);
    this.queryParamsData = jsonObject;
    console.log(this.queryParamsData);
  }

  ngOnInit(): void {
    this.closeSidebar();
this.getList();

  }
  logOut() {
    sessionStorage.setItem('token', '');
    return this.router.navigate(['/login']);
  }
  closeSidebar(): void {
    this.variableShareService.setCloseSidebar(true);
  }
  onPageIndexChange(pageIndex: any) {

    this.offset = pageIndex === 1 ?
      0 : (pageIndex - 1) * this.pageSize;

    this.getList()
  }

  onPageSizeChange(pageSize: any) {
    this.pageSize = pageSize
    this.pageIndex = 1

    this.offset = 0

    this.getList()
  }

  async getList() {
    const messageId = this.message.loading('Loading...').messageId;
    try {
      const _limit = this.pageSize;
      const _offset = this.offset;
      const response = await this.doctorService.getPatientInfo(this.queryParamsData);

      const data: any = response.data;
      this.patientList=data.data;
      console.log(this.patientList);
      
      this.hn = this.patientList.hn;
      this.an = this.patientList.an;
      this.title=this.patientList.patient.title;
      this.fname=this.patientList.patient.fname;
      this.lname=this.patientList.patient.lname;
      this.gender=this.patientList.patient.gender;
      this.inscl_name=this.patientList.inscl_name;
      this.age=this.patientList.patient.age;
      this.address=this.patientList.address;
      this.phone=this.patientList.phone;
      this.chief_complaint=this.patientList.opd_review.chief_complaint;
      this.present_illness=this.patientList.opd_review.present_illness;
      this.past_history=this.patientList.opd_review.past_history;
      this.physical_exam=this.patientList.opd_review.physical_exam;
      this.body_temperature=this.patientList.opd_review.body_temperature;
      this.body_weight=this.patientList.opd_review.body_weight;
      this.body_height=this.patientList.opd_review.body_height;
      this.waist=this.patientList.opd_review.waist;
      this.pulse_rate=this.patientList.opd_review.pulse_rate;
      this.respiratory_rate=this.patientList.opd_review.respiratory_rate;
      this.systolic_blood_pressure=this.patientList.opd_review.systolic_blood_pressure;
      this.diatolic_blood_pressure=this.patientList.opd_review.diatolic_blood_pressure;
      this.oxygen_sat=this.patientList.opd_review.oxygen_sat;
      this.eye_score=this.patientList.opd_review.eye_score;
      this.movement_score=this.patientList.opd_review.movement_score;
      this.verbal_score=this.patientList.opd_review.verbal_score;
      this.bmi=this.body_weight/((this.body_height/100)*(this.body_height/100));



      this.total = await data.total || 1

      this.dataSet = data.data.map((v: any) => {
        const date = v.admit_date ? DateTime.fromISO(v.admit_date).setLocale('th').toLocaleString(DateTime.DATETIME_SHORT_WITH_SECONDS) : '';
        v.admit_date = date;
        return v;
      });
      this.message.remove(messageId);
    } catch (error: any) {
      this.message.remove(messageId);
      this.message.error(`${error.code} - ${error.message}`);
    }
  }
}
