import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DateTime } from 'luxon';
import { DoctorService } from '../services/doctor.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';

@Component({
  selector: 'app-admissionnote',
  templateUrl: './admissionnote.component.html',
  styleUrls: ['./admissionnote.component.css'],
})
export class AdmissionnoteComponent {

  // config page
  query: any = '';
  dataSet: any[] = [];
  loading = false;

  total = 0;
  pageSize = 20;
  pageIndex = 1;
  offset = 0;
  user_login_name: any;
  queryParamsData: any;

  // pateint info
  patientList: any;
  hn: any;
  an: any;
  title: any;
  fname: any;
  lname: any;
  gender: any;
  age: any;
  age_y: any;
  age_m: any;
  age_d: any;
  address: any;
  phone: any;

  // opd review
  chief_complaint: any;
  present_illness: any;
  past_history: any;

  // physical exam
  physical_exam: any;
  body_temperature: any;
  body_weight: any;
  body_height: any;
  waist: any;
  pulse_rate: any;
  respiratory_rate: any;
  systolic_blood_pressure: any;
  diatolic_blood_pressure: any;
  oxygen_sat: any;
  eye_score: any;
  movement_score: any;
  verbal_score: any;
  bmi: any;

  // past history
  is_chronic: any;
  chronic_describe: any;
  is_allergy: any;
  allergy_describe: any;
  is_operation: any;
  operation_describe: any;
  is_family_disease: any;
  family_disease_describe: any;
  last_menstrual_period: any;

  // history pregnant
  gravida: any;
  para: any;
  abortion: any;
  live_birth: any;
  gestational_age: any;
  is_birth_control: any;
  birth_control_describe: any;

  // history child
  is_child_vaccine_complete: any;

  // drug history
  is_smoke: any;
  smoke_describe: any;
  smoke_describe_duration: any;
  smoke_describe_qty: any;
  smoke_describe_frequency: any;
  is_drink_alchohol: any;
  drink_alchohol_describe: any;
  drink_alchohol_describe_duration: any;
  drink_alchohol_describe_qty: any;
  drink_alchohol_describe_frequency: any;

  is_child_development_normal: any;

  // physical examination
  pheent: any;
  pheart: any;
  plung: any;
  pabdomen: any;
  pback_and_cva: any;
  pextremities_and_skin: any;
  pneuro_signs: any;
  general_appearance: any;

  // review of system
  heent: any;
  heart: any;
  lung: any;
  abdomen: any;
  back_and_cva: any;
  extremities_and_skin: any;
  neuro_signs: any;

  // diagnosis && plan of treatment
  diagnosis: string = '';
  plan_of_treatment: string = '';

  modify_date: any;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private doctorService: DoctorService,
    private message: NzMessageService,
    private modal: NzModalService
  ) {
    let jsonString: any =
      this.activatedRoute.snapshot.queryParamMap.get('data');
    const jsonObject = JSON.parse(jsonString);
    this.queryParamsData = jsonObject;
    console.log(this.queryParamsData);
  }

  onChange(result: Date): void {
    console.log('onChange: ', result);
  }

  // get patient info on init
  ngOnInit(): void {
    this.getList();
  }

  // logout
  logOut() {
    sessionStorage.setItem('token', '');
    return this.router.navigate(['/login']);
  }

  // back to home
  onPageIndexChange(pageIndex: any) {
    this.offset = pageIndex === 1 ? 0 : (pageIndex - 1) * this.pageSize;

    this.getList();
  }

  onPageSizeChange(pageSize: any) {
    this.pageSize = pageSize;
    this.pageIndex = 1;

    this.offset = 0;
    this.getList();
  }

  // get patient info
  async getList() {
    const messageId = this.message.loading('Loading...').messageId;
    try {
      const _limit = this.pageSize;
      const _offset = this.offset;
      const response = await this.doctorService.getPatientInfo(
        this.queryParamsData
      );

      const data: any = response.data;

      // set data to patient info
      this.patientList = await data.data;
      console.log(this.patientList);
      this.hn = this.patientList.hn;
      this.an = this.patientList.an;
      this.title = this.patientList.patient.title;
      this.fname = this.patientList.patient.fname;
      this.lname = this.patientList.patient.lname;
      this.gender = this.patientList.patient.gender;
      // this.age = this.patientList.patient.age;
      this.address = this.patientList.patient.address;
      this.phone = this.patientList.patient.phone;
      this.chief_complaint = this.patientList.opd_review.chief_complaint;
      this.present_illness = this.patientList.opd_review.present_illness;
      this.past_history = this.patientList.opd_review.past_history;
      this.physical_exam = this.patientList.opd_review.physical_exam;
      this.body_temperature = this.patientList.opd_review.body_temperature;
      this.body_weight = this.patientList.opd_review.body_weight;
      this.body_height = this.patientList.opd_review.body_height;
      this.waist = this.patientList.opd_review.waist;
      this.pulse_rate = this.patientList.opd_review.pulse_rate;
      this.respiratory_rate = this.patientList.opd_review.respiratory_rate;
      this.systolic_blood_pressure = this.patientList.opd_review.systolic_blood_pressure;
      this.diatolic_blood_pressure = this.patientList.opd_review.diatolic_blood_pressure;
      this.oxygen_sat = this.patientList.opd_review.oxygen_sat;
      this.eye_score = this.patientList.opd_review.eye_score;
      this.movement_score = this.patientList.opd_review.movement_score;
      this.verbal_score = this.patientList.opd_review.verbal_score;
      
      // split age to year, month, day
      let age = this.patientList.patient.age;
      let ageSplit = age.split('-');
      this.age_y = ageSplit[0];
      this.age_m = ageSplit[1];
      this.age_d = ageSplit[2];
      
      if(Number(this.age_y) == 0){
        this.age = Number(this.age_m) + ' เดือน ' + Number(this.age_d) + ' วัน';
      } else {
        this.age = Number(this.age_y) + ' ปี ';
      }

      // check if value is zero
      if(this.body_height  != 0 && this.body_weight != 0){
        this.bmi = this.body_weight / ((this.body_height / 100) * (this.body_height / 100));
      } else {
        this.bmi = 0;
      }

      this.total = data.total || 1;

      this.dataSet = data.data.map((v: any) => {
        const date = v.admit_date
          ? DateTime.fromISO(v.admit_date)
              .setLocale('th')
              .toLocaleString(DateTime.DATETIME_SHORT_WITH_SECONDS)
          : '';
        v.admit_date = date;
        return v;
      });
      this.message.remove(messageId);
    } catch (error: any) {
      this.message.remove(messageId);
      this.message.error(`${error.code} - ${error.message}`);
    }
  }

  // save admission note
  async saveAdmissionNote() {
    const currentDate = new Date();
    const year = currentDate.getFullYear();
    const month = String(currentDate.getMonth() + 1).padStart(2, '0'); // Month is zero-based, so add 1
    const day = String(currentDate.getDate()).padStart(2, '0');

    const formattedDate = `${year}-${month}-${day}`;
    console.log(formattedDate);

    // set data to admission note
    let datat: any = {
      admission_note: {
        admit_id: this.queryParamsData,
        pre_diagnosis: 'E10',
        plan_of_treatment: '',
        review_of_system: {
          heent: 'false',
          heart: 1,
          lung: false,
          abdomen: true,
          back_and_cva: true,
          extremities_and_skin: false,
          neuro_signs: true,
        },
        physical_examination: {
          heent: false,
          heart: true,
          lung: false,
          abdomen: true,
          back_and_cva: true,
          extremities_and_skin: true,
          neuro_signs: true,
          general_appearance: true,
        },
      },
      patient_history: {
        admit_id: this.queryParamsData,
        is_chronic: false,
        chronic_describe: '',
        is_allergy: true,
        allergy_describe: '',
        is_operation: true,
        operation_describe: '',
        is_family_disease: true,
        family_disease_describe: '',
        last_menstrual_period: '2023-09-12',
        is_child_development_normal: true,
        is_child_vaccine_complete: true,
        is_smoke: true,
        is_drink_alchohol: true,
        smoke_describe: {
          duration: '',
          qty: '',
          frequency: '',
        },
        drink_alchohol_describe: {
          duration: '',
          qty: '',
          frequency: '',
        },
        reference_by: '',
        modify_date: '2023-09-12',
      },
      patient_pregnant: {
        admit_id: this.queryParamsData,
        gravida: 1,
        para: 0,
        abortion: 1,
        live_birth: 0,
        note: '',
      },
    };

    let data: any = {
      admission_note: {
        admit_id: this.queryParamsData,
        pre_diagnosis: this.diagnosis,
        plan_of_treatment: this.plan_of_treatment,
        review_of_system: {
          heent: this.heent,
          heart: this.heart,
          lung: this.lung,
          abdomen: this.abdomen,
          back_and_cva: this.back_and_cva,
          extremities_and_skin: this.extremities_and_skin,
          neuro_signs: this.neuro_signs,
        },
        physical_examination: {
          heent: this.heent,
          heart: this.heart,
          abdomen: this.abdomen,
          back_and_cva: this.back_and_cva,
          extremities_and_skin: this.extremities_and_skin,
          neuro_signs: this.neuro_signs,
          general_appearance: this.general_appearance,
        },
      },
      patient_history: {
        admit_id: this.queryParamsData,
        is_chronic: this.is_chronic,
        chronic_describe: this.chronic_describe,
        is_allergy: this.is_allergy,
        allergy_describe: this.allergy_describe,
        is_operation: this.is_operation,
        operation_describe: this.operation_describe,
        is_family_disease: this.family_disease_describe,
        family_disease_describe: this.family_disease_describe,
        last_menstrual_period: this.last_menstrual_period,
        is_child_vaccine_complete: this.is_child_vaccine_complete,
        is_smoke: this.is_smoke,
        is_drink_alchohol: this.is_drink_alchohol,
        smoke_describe: {
          duration: this.smoke_describe_duration,
          qty: this.smoke_describe_qty,
          frequency: this.smoke_describe_frequency,
        },
        drink_alchohol_describe: {
          duration: this.drink_alchohol_describe_duration,
          qty: this.drink_alchohol_describe_qty,
          frequency: this.drink_alchohol_describe_frequency,
        },
        reference_by: '',
        modify_date: formattedDate,
      },
      patient_pregnant: {
        admit_id: this.queryParamsData,
        gravida: this.gravida,
        para: this.para,
        abortion: this.abortion,
        live_birth: this.live_birth,
        note: '',
      },
    };

    console.log(data);

    try {
      let rs = await this.doctorService.saveAdmissionNote(data);
      console.log(rs);

      // this.message.remove(messageId);
      // this.message.success('ยกเลิกรายการเรียบร้อย');
      // this.getList();
    } catch (error: any) {
      console.log(error);

      // this.message.remove(messageId);
      // this.message.error(`${error.code} - ${error.message}`);
    }
  }

  // reset form input 
  resetForm() {
    this.chronic_describe = '';
    this.allergy_describe = '';
    this.operation_describe = '';
    this.family_disease_describe = '';
    this.last_menstrual_period = '';
    this.smoke_describe = '';
    this.smoke_describe_duration = '';
    this.smoke_describe_qty = '';
    this.smoke_describe_frequency = '';
    this.drink_alchohol_describe = '';
    this.drink_alchohol_describe_duration = '';
    this.drink_alchohol_describe_qty = '';
    this.drink_alchohol_describe_frequency = '';
    this.gravida = '';
    this.para = '';
    this.abortion = '';
    this.live_birth = '';
    this.is_allergy = false;
    this.is_operation = false;
    this.is_family_disease = false;
    this.is_child_vaccine_complete = false;
    this.is_smoke = false;
    this.is_drink_alchohol = false;
    this.is_child_development_normal = false;
    this.heent = false;
    this.heart = false;
    this.lung = false;
    this.abdomen = false;
    this.back_and_cva = false;
    this.extremities_and_skin = false;
    this.neuro_signs = false;
    this.pheent = false;
    this.pheart = false;
    this.plung = false;
    this.pabdomen = false;
    this.pback_and_cva = false;
    this.pextremities_and_skin = false;
    this.pneuro_signs = false;
    this.general_appearance = false;



  }
}
